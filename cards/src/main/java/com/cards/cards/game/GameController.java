package com.cards.cards.game;

import com.cards.cards.player.Player;
import com.cards.cards.player.PlayerNameAndScore;
import com.cards.cards.round.Round;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/api/game")
public class GameController {

    @Autowired
    public GameService gameService;

    @Autowired private SimpMessagingTemplate simpMessagingTemplate;

    // Create a new game
    @PostMapping(value = "/{rounds}")
    public ResponseEntity<Game> chooseBoxes(@RequestBody List<String> boxes, @PathVariable("rounds") Integer rounds) {
        return new ResponseEntity<>(gameService.newGame(boxes, rounds), HttpStatus.ACCEPTED);
    }

    @PutMapping(value = "/addplayer/{gameid}")
    public ResponseEntity addPlayerToGame(@RequestBody List<Long> ids, @PathVariable("gameid") Long id) {
        gameService.addPlayerToGame(ids, id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PostMapping(value = "/addplayer/{gameid}")
    public ResponseEntity<Player> addOnePlayerToGame(@RequestBody String name, @PathVariable("gameid") String gamecode) {
        System.out.println(name);
        return new ResponseEntity<>(gameService.addSinglePlayerToGame(name, gamecode), HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/getplayers/{gameid}")
    public ResponseEntity<List<Player>> getPlayersFromGame(@PathVariable("gameid") String id) {
        return new ResponseEntity<>(gameService.getPlayersFromGame(id), HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/newround/{gameid}")
    public ResponseEntity<Round> newRound(@PathVariable("gameid") String id) {
        return new ResponseEntity<>(gameService.newRound(id), HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/getgame/{gamecode}")
    public ResponseEntity<Game> getGame(@PathVariable("gamecode") String gamecode) {
        return new ResponseEntity<>(gameService.getGame(gamecode), HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/startgame/{gameid}")
    public ResponseEntity<Game> startGame(@PathVariable("gameid") String id) {
        return new ResponseEntity<>(gameService.startGame(id), HttpStatus.ACCEPTED);
    }

    @MessageMapping("/game.players.{gamename}")
    @SendTo("/game/players.{gamename}")
    public List<Player> getPlayersFromGame(@DestinationVariable("gamename") String gamename, Principal principal) throws Exception {
        return gameService.getPlayersFromGame(gamename);
    }

    @MessageMapping("/game.clientlobby.{gamename}")
    @SendTo("/game/clientlobby.{gamename}")
    public GameMinified getMinifiedGame(@DestinationVariable("gamename") String gamename, Principal principal) throws Exception {
        return gameService.getMinifiedGame(gamename);
    }

    @GetMapping(value = "/getminigame/{gamecode}")
    public GameMinified getMinifiedGameGet(@PathVariable("gamecode") String gamecode) throws Exception {
        return gameService.getMinifiedGame(gamecode);
    }

    //TODO skicka meddelande till denna socketen när det blir ny score.

    @MessageMapping("/game.gamescore.{gamename}")
    @SendTo("/game/gamescore.{gamename}")
    public List<PlayerNameAndScore> getScoreListWebsocket(@DestinationVariable("gamename") String gamename, Principal principal) throws Exception {
        return gameService.getGameScores(gamename);
    }

    @GetMapping(value = "/getscorelist/{gamecode}")
    public List<PlayerNameAndScore> getScoreList(@PathVariable("gamecode") String gamecode) throws Exception {
        return gameService.getGameScores(gamecode);
    }
}
