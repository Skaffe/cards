package com.cards.cards.whitecard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class WhiteCardService {

    @Autowired
    WhiteCardRepository whiteCardRepository;

    public WhiteCard newCard(WhiteCard card) {
        return whiteCardRepository.save(card);
    }

    public List<WhiteCard> findAll() {
        return whiteCardRepository.findAll();
    }

    public Optional<WhiteCard> findOne(Long id) {
        return whiteCardRepository.findById(id);
    }

    public void insertCardList(List<String> cards) {
        int total = 0;
        for (String card : cards) {
            WhiteCard wc = new WhiteCard();
            wc.setText(card);
            whiteCardRepository.save(wc);
            total++;
        }
        System.out.println("Inserted: " + total + " whitecards");
    }

    public Set<String> getAllBoxNames() {
        List<WhiteCard> list = whiteCardRepository.findAll();
        Set<String> names = new HashSet<>();

        for (WhiteCard wc : list) {
            names.add(wc.getBox());
        }

        return names;
    }

}
