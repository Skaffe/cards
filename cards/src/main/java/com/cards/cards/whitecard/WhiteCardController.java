package com.cards.cards.whitecard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping(value = "/api/whitecard")
public class WhiteCardController {

    @Autowired
    private WhiteCardService cardService;

//    @PostMapping
//    public ResponseEntity insertCard(@RequestBody WhiteCard card) {
//        cardService.newCard(card);
//        return new ResponseEntity<>(card, HttpStatus.ACCEPTED);
//    }

    @PostMapping
    public ResponseEntity insertCardList(@RequestBody List<String> cards){
        cardService.insertCardList(cards);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<WhiteCard> getAllCards() {
        return cardService.findAll();
    }


    @GetMapping(value = "/{id}")
    public Optional<WhiteCard> findOne(@PathVariable("id") Long id) {
        System.out.println(id);

        return cardService.findOne(id);
    }

    @GetMapping("/boxes")
    public Set<String> getBoxNames(){
        return cardService.getAllBoxNames();
    }
}
