package com.cards.cards.deck;

import com.cards.cards.blackcard.BlackCard;
import com.cards.cards.game.Game;
import com.cards.cards.whitecard.WhiteCard;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Deck {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToMany
    @JoinTable(name = "whiteCardsInDeck",
            joinColumns = {@JoinColumn(name = "deck_id")},
            inverseJoinColumns = {@JoinColumn(name = "whitecard_id")}
    )
    private List<WhiteCard> whiteCardsInDeck = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "blackCardsInDeck",
            joinColumns = {@JoinColumn(name = "deck_id")},
            inverseJoinColumns = {@JoinColumn(name = "blackcard_id")}
    )
    private List<BlackCard> blackCardsInDeck = new ArrayList<>();

}
