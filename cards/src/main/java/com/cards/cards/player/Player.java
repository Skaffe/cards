package com.cards.cards.player;

import com.cards.cards.whitecard.WhiteCard;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Player {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String userName;

    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinTable(name = "cardsInHand",
            joinColumns = {@JoinColumn(name = "player_id")},
            inverseJoinColumns = {@JoinColumn(name = "whitecard_id")}
    )
    private List<WhiteCard> cardsInHand = new ArrayList<>();

    private Boolean judge = false;

    private Integer score = 0;

    public void addCardToHand(WhiteCard wc) {
        this.cardsInHand.add(wc);
    }

    public void removeCardFromHand(WhiteCard wc) {
        this.cardsInHand.remove(wc);
    }

    public Player(String userName) {
        score = 0;
        this.userName = userName;
    }
}
