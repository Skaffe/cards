package com.cards.cards.playedcard;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.cards.cards.player.Player;
import com.cards.cards.whitecard.WhiteCard;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class PlayedCard {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToMany
    @JoinTable(name = "playedCards",
            joinColumns = {@JoinColumn(name = "playedcard_id")},
            inverseJoinColumns = {@JoinColumn(name = "wc_id")}
    )
    private List<WhiteCard> playedCards = new ArrayList<>();

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "player_id")
    public Player player;

    public void addWcToList(WhiteCard wc) {
        playedCards.add(wc);
    }
}
