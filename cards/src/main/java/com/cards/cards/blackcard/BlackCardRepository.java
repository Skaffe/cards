package com.cards.cards.blackcard;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Set;

public interface BlackCardRepository extends CrudRepository<BlackCard, Long> {
    Set<BlackCard> findAllByBox(String s);

    List<BlackCard> findAll();
}
