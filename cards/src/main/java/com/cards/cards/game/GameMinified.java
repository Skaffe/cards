package com.cards.cards.game;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class GameMinified {
    public String roomCode;
    public String judge;
    public Boolean started = false;
}
