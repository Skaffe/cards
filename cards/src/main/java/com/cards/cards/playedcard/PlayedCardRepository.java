package com.cards.cards.playedcard;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayedCardRepository extends CrudRepository<PlayedCard, Long> {

}