package com.cards.cards.game;

import com.cards.cards.deck.Deck;
import com.cards.cards.deck.DeckRepository;
import com.cards.cards.player.Player;
import com.cards.cards.player.PlayerNameAndScore;
import com.cards.cards.player.PlayerRepository;
import com.cards.cards.blackcard.BlackCard;
import com.cards.cards.blackcard.BlackCardRepository;
import com.cards.cards.round.Round;
import com.cards.cards.round.RoundRepository;
import com.cards.cards.whitecard.WhiteCard;
import com.cards.cards.whitecard.WhiteCardRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class GameService {
    @Autowired
    GameRepository gameRepository;

    @Autowired
    WhiteCardRepository whiteCardRepository;

    @Autowired
    BlackCardRepository blackCardRepository;

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    RoundRepository roundRepository;

    @Autowired
    SimpMessagingTemplate messagingTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    DeckRepository deckRepository;

    final Long HAND_SIZE = 7L;

    public Game newGame(List<String> boxes, Integer roundsToWin) {
        Game newGame = new Game();

        List<WhiteCard> whiteCards = new ArrayList<>();
        List<BlackCard> blackCards = new ArrayList<>();
        Deck newDeck = new Deck();

        for (String s : boxes) {
            for (WhiteCard card : whiteCardRepository.findAllByBox(s)) {
                whiteCards.add(card);
            }
            for (BlackCard card : blackCardRepository.findAllByBox(s)) {
                blackCards.add(card);
            }
        }

        newDeck.setWhiteCardsInDeck(whiteCards);
        newDeck.setBlackCardsInDeck(blackCards);

        newGame.setDeck(newDeck);
        newGame.setRoomCode(generateRoomCode());
        newGame.setRoundsToWin(roundsToWin);
        newGame.setActive(true);

        gameRepository.save(newGame);

        return newGame;
    }

    public Game startGame(String gameId) {
        Optional<Game> game = gameRepository.findGameByRoomCode(gameId);
        Deck deck = game.get().getDeck();
        Random rand = new Random();

        if (game.get().getPlayers().size() > 2) {
            Integer user = rand.nextInt(game.get().getPlayers().size());
            Player player = game.get().getPlayers().get(user);
            player.setJudge(true);

            playerRepository.save(player);

            drawCards(gameId);

            Round round = new Round();
            round.setJudge(player);

            game.get().addRoundToGame(round);
            game.get().setStarted(true);

            BlackCard newBlackCard = drawBlackCard(game.get());

            round.setBlackCard(newBlackCard);
            deck.getBlackCardsInDeck().remove(newBlackCard);

            roundRepository.save(round);
            gameRepository.save(game.get());

//            deckRepository.deleteByBlackCardsInDeckIn(newBlackCard);

            this.messagingTemplate.convertAndSend("/game/clientlobby." + game.get().getRoomCode(), getMinifiedGame(game.get().getRoomCode()));

            return game.get();
        } else {
            System.out.println("not enough players");
        }
        return null;
    }


    public void drawCards(String gameId) {
        Optional<Game> game = gameRepository.findGameByRoomCode(gameId);

        Deck deck = game.get().getDeck();
        Random rand = new Random();
        for (Player player : game.get().getPlayers()) {
            while (player.getCardsInHand().size() < HAND_SIZE) {
                int card = rand.nextInt(deck.getWhiteCardsInDeck().size());

                player.addCardToHand(deck.getWhiteCardsInDeck().get(card));
                deck.getWhiteCardsInDeck().remove(card);
            }
        }
        gameRepository.save(game.get());
    }

    public BlackCard drawBlackCard(Game game) {
        Deck deck = game.getDeck();

        Random rand = new Random();
        int card = rand.nextInt(deck.getBlackCardsInDeck().size() - 1);
        BlackCard bc = deck.getBlackCardsInDeck().get(card);

        return bc;
    }

    public void addPlayerToGame(List<Long> ids, Long gameid) {
        Optional<Game> game = gameRepository.findById(gameid);
        List<Player> players = new ArrayList<>();
        for (Long id : ids) {
            Optional<Player> player = playerRepository.findById(id);

            players.add(player.get());

        }
        game.get().setPlayers(players);
        gameRepository.save(game.get());
    }

    public Player addSinglePlayerToGame(String name, String gamecode) {
        Game game = gameRepository.findGameByRoomCode(gamecode.toUpperCase()).get();
        if (game != null) {
            boolean exists = false;
            for (Player player1 : game.getPlayers()) {
                if (name.equalsIgnoreCase(player1.getUserName())) {
                    exists = true;
                }
            }
            Player player = new Player(name);
            if (exists) {
                Random r = new Random();
                player.setUserName("UniqueUsername" + r.nextInt((10000) + 1));
            }

            playerRepository.save(player);
            game.addPlayerToGame(player);

            gameRepository.save(game);

            this.messagingTemplate.convertAndSend("/game/players." + gamecode, game.getPlayers());

            return player;
        }
        return null;
    }

    public List<Player> getPlayersFromGame(String gamecode) {
        Game game = gameRepository.findGameByRoomCode(gamecode.toUpperCase()).orElse(null);

        if (game != null) {
            return game.getPlayers();
        }
        return null;
    }

    public Game getGame(String gamecode) {
        Game game = gameRepository.findGameByRoomCode(gamecode.toUpperCase()).get();

        if (game != null) {
            return game;
        }
        return null;
    }

    private String generateRoomCode() {
        Random rand = new Random();
        String letters = "QWERTYUIOPASDFGHJKLZXCVBNM";
        String code = "";
        for (int i = 0; i < 5; i++) {
            char c = letters.charAt(rand.nextInt(letters.length()));
            code += c;
        }
        return code;
    }

    public Round newRound(String gameid) {
        Optional<Game> game = gameRepository.findGameByRoomCode(gameid);
        Deck deck = game.get().getDeck();

        Round round = new Round();

        BlackCard newBlackCard = drawBlackCard(game.get());

        round.setBlackCard(newBlackCard);
        deck.getBlackCardsInDeck().remove(newBlackCard);

        drawCards(gameid);

        List<Player> players = game.get().getPlayers();

        for (int i = 0; i < players.size(); i++) {
            if (players.get(i).getJudge()) {
                players.get(i).setJudge(false);
                round.setJudge(players.get(0));

                if (i == players.size() - 1) {
                    players.get(0).setJudge(true);
                    round.setJudge(players.get(0));
                    break;
                } else {
                    players.get(i + 1).setJudge(true);
                    round.setJudge(players.get(i + 1));
                    break;
                }
            }
        }
        game.get().addRoundToGame(round);
        roundRepository.save(round);
        gameRepository.save(game.get());
        this.messagingTemplate.convertAndSend("/game/round." + game.get().getRoomCode(), game.get().getLatestRound());
        return round;
    }

    public List<PlayerNameAndScore> getGameScores(String gamename) {
        Game game = gameRepository.findGameByRoomCode(gamename).get();
        List<PlayerNameAndScore> scores = new ArrayList<>();


        for (Player player : game.getPlayers()) {
            scores.add(new PlayerNameAndScore(player.getUserName(), player.getScore()));
        }

        return scores;
    }


    public GameMinified getMinifiedGame(String gamename) {
        Optional<Game> game = gameRepository.findGameByRoomCode(gamename);
        String judge = null;
        for (Player player : game.get().getPlayers()) {
            if (player.getJudge() == true) {
                judge = player.getUserName();
            }
        }

        return new GameMinified(game.get().getRoomCode(), judge, game.get().getStarted());

    }
}
