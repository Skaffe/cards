package com.cards.cards.player;

import com.cards.cards.game.Game;
import com.cards.cards.game.GameRepository;
import com.cards.cards.whitecard.WhiteCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PlayerService {
    @Autowired
    PlayerRepository playerRepo;

    @Autowired
    GameRepository gameRepository;

    public List<WhiteCard> getHand(String gamecode, String name) {
        Optional<Game> game = gameRepository.findGameByRoomCode(gamecode);

        for(Player player : game.get().getPlayers()){
            if(name.equals(player.getUserName())){
                return player.getCardsInHand();
            }
        }
        return null;
//        return playerRepo.findById(id).get().getCardsInHand();
    }

    public Player newPlayer(Player player) {
        player.setScore(0);
        return playerRepo.save(player);
    }
}
