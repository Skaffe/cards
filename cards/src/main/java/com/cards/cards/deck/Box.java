package com.cards.cards.deck;

import lombok.*;

import java.util.List;

@Getter
@ToString
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Box {
    private String name;

    private List<Long> black;

    private List<Long> white;

    private String icon;
}
