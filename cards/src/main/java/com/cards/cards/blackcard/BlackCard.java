package com.cards.cards.blackcard;

import lombok.*;

import javax.persistence.*;


@Entity
@Getter
@ToString
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BlackCard{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String text;
    private String box;
    private Long pick;

}
