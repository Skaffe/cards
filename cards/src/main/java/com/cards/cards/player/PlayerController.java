package com.cards.cards.player;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.HtmlUtils;

@RestController
@RequestMapping(value = "/api/player")
public class PlayerController {
    @Autowired
    PlayerService playerService;

    @PostMapping
    public ResponseEntity<Player> createNewPlayer(@RequestBody Player player) {
        return new ResponseEntity<>(playerService.newPlayer(player), HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/gethand/gamecode/{gamecode}/username/{name}")
    public ResponseEntity getHand(@PathVariable("gamecode") String gamecode, @PathVariable("name") String name) {
        return new ResponseEntity<>(playerService.getHand(gamecode, name), HttpStatus.ACCEPTED);
    }


}
