import Vue from 'vue'
import App from './components/App.vue'
import RootView from './components/Root.vue'
import Lobby from './components/Lobby.vue'
import Game from './components/Game.vue'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import VueRouter from 'vue-router'
Vue.use(Buefy)
Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', component: RootView },
    { path: '/game/:roomcode', name: 'lobby', component: Lobby},
    { path: '/:roomcode', name: 'game', component: Game}
  ]
})

new Vue({
  render: h => h(App),

  router
}).$mount('#app')
