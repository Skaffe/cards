package com.cards.cards.game;

import com.cards.cards.deck.Deck;
import com.cards.cards.player.Player;
import com.cards.cards.round.Round;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL, fetch= FetchType.LAZY)
    @JoinColumn(name = "deck_id")
    private Deck deck;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "playersInGame",
            joinColumns = {@JoinColumn(name = "game_id")},
            inverseJoinColumns = {@JoinColumn(name = "player_id")}
    )
    private List<Player> players = new ArrayList<>();

    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "roundsInGame",
            joinColumns = {@JoinColumn(name = "game_id")},
            inverseJoinColumns = {@JoinColumn(name = "round_id")})
    private List<Round> roundsInGame = new ArrayList<>();

    private String roomCode;

    public Integer roundsToWin;

    private Boolean active;

    @OneToOne
    @JoinColumn(name = "winner_player_id")
    private Player winner;

    private Boolean started = false;

    public void addPlayerToGame(Player player) {
        this.players.add(player);
    }

    public void addRoundToGame(Round round) {
        this.roundsInGame.add(round);
    }

    public Round getLatestRound() {
        if (roundsInGame != null && !roundsInGame.isEmpty())
            return roundsInGame.get(getRoundsInGame().size() - 1);
        else {
            return null;
        }
    }
}
