package com.cards.cards.deck;

import com.cards.cards.blackcard.BlackCard;
import com.cards.cards.blackcard.BlackCardRepository;
import com.cards.cards.whitecard.WhiteCard;
import com.cards.cards.whitecard.WhiteCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DeckService {

    @Autowired
    DeckRepository deckRepository;

    @Autowired
    WhiteCardRepository whiteCardRepository;

    @Autowired
    BlackCardRepository blackCardRepository;

    public void all(List<Box> cards){
        System.out.println(cards);
        for(Box box : cards){
            for(Long blackCard : box.getBlack()){
                Optional<BlackCard> findBlackCard = blackCardRepository.findById(blackCard+1);
                findBlackCard.get().setBox(box.getName());
                blackCardRepository.save(findBlackCard.get());
            }
            for(Long whiteCard : box.getWhite()){
                Optional<WhiteCard> findWhiteCard = whiteCardRepository.findById(whiteCard+1);
                findWhiteCard.get().setBox(box.getName());
                whiteCardRepository.save(findWhiteCard.get());
            }
        }

    }
}
