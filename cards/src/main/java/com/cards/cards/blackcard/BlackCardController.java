package com.cards.cards.blackcard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/blackcard")
public class BlackCardController {

    @Autowired
    private BlackCardService cardService;

//    @PostMapping
//    public ResponseEntity insertCard(@RequestBody BlackCard card) {
//        cardService.newCard(card);
//        return new ResponseEntity<>(card, HttpStatus.ACCEPTED);
//    }

    @PostMapping
    public ResponseEntity insertCardList(@RequestBody List<BlackCard> cards){
        cardService.insertCardList(cards);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
