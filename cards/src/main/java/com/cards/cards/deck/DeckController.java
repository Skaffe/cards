package com.cards.cards.deck;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/deck")
public class  DeckController {
    @Autowired
    private DeckService deckService;

    @PostMapping
    public ResponseEntity insertCardList(@RequestBody List<Box> cards){
        deckService.all(cards);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

}
