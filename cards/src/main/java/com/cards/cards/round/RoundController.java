package com.cards.cards.round;

import com.cards.cards.playedcard.PlayedCard;
import com.cards.cards.player.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api/round")
public class RoundController {
    @Autowired
    RoundService roundService;

    @PostMapping("/gamecode/{gamecode}/username/{username}")
    public String postCardToRound(@PathVariable("gamecode") String gamecode, @PathVariable("username") String username, @RequestBody Long[] whitecardIds){
        System.out.println(username);
        roundService.postCardToRound(gamecode, username, whitecardIds);

        return "Done";
    }

    @GetMapping("/getplayedcards/gameid/{gameid}")
    public ResponseEntity<List<PlayedCard>> showCards(@PathVariable Long gameid){
        return new ResponseEntity<>(roundService.getPlayedCards(gameid), HttpStatus.ACCEPTED);
    }

    @GetMapping("/getlatestround/gameid/{gameid}")
    public ResponseEntity<Round> getLatestRound(@PathVariable String gameid){
        return new ResponseEntity<>(roundService.getRound(gameid), HttpStatus.ACCEPTED);
    }

    //todo säg till new round message
    @PostMapping("/winner/gameid/{game_id}/playedcardid/{playedcard}")
    public Round chooseWinner(@PathVariable String game_id, @PathVariable Long playedcard){
        return roundService.chooseWinner(game_id, playedcard);
    }


    @MessageMapping("/game.round.{gamename}")
    @SendTo("/game/round.{gamename}")
    public ResponseEntity<Round>  getLatestRound(@DestinationVariable("gamename") String gamename, Principal principal) throws Exception {
        return new ResponseEntity<>(roundService.getRound(gamename), HttpStatus.ACCEPTED);
    }
}
