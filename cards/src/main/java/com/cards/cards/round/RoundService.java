package com.cards.cards.round;

import com.cards.cards.game.Game;
import com.cards.cards.game.GameRepository;
import com.cards.cards.game.GameService;
import com.cards.cards.playedcard.PlayedCard;
import com.cards.cards.playedcard.PlayedCardRepository;
import com.cards.cards.player.Player;
import com.cards.cards.player.PlayerRepository;
import com.cards.cards.whitecard.WhiteCard;
import com.cards.cards.whitecard.WhiteCardRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class RoundService {
    @Autowired
    public RoundRepository roundRepository;

    @Autowired
    public GameService gameService;

    @Autowired
    public GameRepository gameRepository;

    @Autowired
    public PlayerRepository playerRepository;

    @Autowired
    public WhiteCardRepository whiteCardRepository;

    @Autowired
    public PlayedCardRepository playedCardRepository;

    @Autowired
    SimpMessagingTemplate messagingTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    public void postCardToRound(String gameId, String name, Long[] whitecardIds) {
        Optional<Game> game = gameRepository.findGameByRoomCode(gameId);
        Long playerId = null;

        for (Player player1 : game.get().getPlayers()) {
            if (name.equalsIgnoreCase(player1.getUserName())) {
                playerId = player1.getId();
            }
        }

        Optional<Player> player = playerRepository.findById(playerId);

        List<WhiteCard> listOfCards = new ArrayList();

        Round currentRound = game.get().getLatestRound();

        if (!player.get().getJudge()) {
            for (Long card : whitecardIds) {
                Optional<WhiteCard> wCard = whiteCardRepository.findById(card);
                listOfCards.add(wCard.get());
                player.get().removeCardFromHand(wCard.get());
            }

            PlayedCard pc = new PlayedCard();
            pc.setPlayedCards(listOfCards);
            pc.setPlayer(player.get());

            currentRound.addCardToRound(pc);


            roundRepository.save(currentRound);
            playerRepository.save(player.get());

            //If everyone has played their cards, judge gets to choose winner
            if (currentRound.cardsPlayed.size() == game.get().getPlayers().size() - 1) {
                currentRound.setFinished(true);
                Map<String, Object> map = objectMapper.convertValue(currentRound, Map.class);
                messagingTemplate.convertAndSend("/game/round." + game.get().getRoomCode(), map);
            }
        }

    }

    public Round chooseWinner(String game_id, Long playedcard) {
        Optional<PlayedCard> playedCard = playedCardRepository.findById(playedcard);
        Optional<Game> game = gameRepository.findGameByRoomCode(game_id);
        Round currentRound = game.get().getLatestRound();

        boolean exists = false;

        for (Player p1 : game.get().getPlayers()) {
            if (p1.getId().equals(playedCard.get().getPlayer().getId())) {
                exists = true;
            }
        }

        if (exists) {
            if (playedCard.get().getPlayer().getScore().equals(null)) {
                playedCard.get().getPlayer().setScore(0);
            }
            playedCard.get().getPlayer().setScore(playedCard.get().getPlayer().getScore() + 1);

            currentRound.setWinner(playedCard.get());

            if (playedCard.get().getPlayer().getScore() == game.get().roundsToWin) {
                game.get().setWinner(playedCard.get().getPlayer());
                System.out.println(game.get().getWinner().getUserName() + " HAS WON");
            } else {
                gameService.newRound(game_id);
            }

            gameRepository.save(game.get());


        } else {
            System.out.println("ERROR CARD NOT FOUND");
        }

        this.messagingTemplate.convertAndSend("/game/gamescore." + game.get().getRoomCode(), gameService.getGameScores(game.get().getRoomCode()));

        return game.get().getLatestRound();
    }

    public List<PlayedCard> getPlayedCards(Long gameid) {
        Optional<Game> game = gameRepository.findById(gameid);
        Round round = game.get().getLatestRound();

        List<PlayedCard> playedCards = round.getCardsPlayed();

        return playedCards;

    }

    public Round getRound(String gamecode) {
        Optional<Game> game = gameRepository.findGameByRoomCode(gamecode);
        for (PlayedCard pc : game.get().getLatestRound().cardsPlayed) {
            System.out.println(pc.getId());
        }
        return game.get().getLatestRound();
    }
}
