package com.cards.cards.blackcard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BlackCardService {

    @Autowired
    BlackCardRepository blackCardRepository;

    public BlackCard newCard(BlackCard card){
        return blackCardRepository.save(card);
    }

    public List<BlackCard> findAll() {
        return blackCardRepository.findAll();
    }

    public Optional<BlackCard> findOne(Long id) {
        return blackCardRepository.findById(id);
    }

    public void insertCardList(List<BlackCard> cards) {
        int total = 0;
        for(BlackCard card : cards){
            blackCardRepository.save(card);
            total++;
        }
        System.out.println("Inserted: " + total + " blackcards");
    }
}
