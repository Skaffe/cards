package com.cards.cards.whitecard;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;
@Repository
public interface WhiteCardRepository extends CrudRepository<WhiteCard, Long> {

    List<WhiteCard> findAll();

    Optional<WhiteCard> findById(Long aLong);

    Set<WhiteCard> findAllByBox(String s);
}
