package com.cards.cards.game;

import com.cards.cards.player.Player;
import org.aspectj.apache.bcel.util.Play;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GameRepository extends CrudRepository<Game, Long> {
    Optional<Game> findGameByRoomCode(String roomcode);
}
