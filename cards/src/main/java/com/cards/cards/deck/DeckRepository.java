package com.cards.cards.deck;

import com.cards.cards.blackcard.BlackCard;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeckRepository extends CrudRepository<Deck, Long> {
    void removeByBlackCardsInDeck(BlackCard bc);
    void deleteByBlackCardsInDeckIn(BlackCard bc);

//    void deleteByBlackCardsInDeckIn()

//    @Query("SELECT d.blackCardsInDeck FROM Deck d Where d.id = ?1 Order by RANDOM() LIMIT 1 ")
//    BlackCard getRandomCard(Long id);
}
