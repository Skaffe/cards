package com.cards.cards.round;

import com.cards.cards.blackcard.BlackCard;
import com.cards.cards.playedcard.PlayedCard;
import com.cards.cards.player.Player;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Round {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Boolean finished = false;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "winner")
    private PlayedCard winner;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "judge")
    private Player judge;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "CardsPlayedInRound",
            joinColumns = @JoinColumn(name = "round_id"),
            inverseJoinColumns = @JoinColumn(name = "playedCard_id")
    )
    List<PlayedCard> cardsPlayed = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "blackcard")
    private BlackCard blackCard;

    public void addCardToRound(PlayedCard pc) {
        cardsPlayed.add(pc);
    }


}
